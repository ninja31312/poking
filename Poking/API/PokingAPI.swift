//
//  GoogleMapAPI.swift
//  Poking
//
//  Created by Ching-Fan Hsieh on 11/26/16.
//  Copyright © 2016 Poking. All rights reserved.
//

import Foundation
import CoreLocation
import Moya

/*
 We have free quota that query the API 1000 times per day. If we exceeded the upper bound, the service will be blocked on that day.
 */

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data //fallback to original data if it cant be serialized
    }
}

let PokingProvider = MoyaProvider<Poking>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

// MARK: - Provider support

private extension String {
    var urlEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum GoogleMap {
    case nearParkingInfo(CLLocationCoordinate2D)
}

public enum Poking {
    case hotel(CLLocationCoordinate2D)
    case pokemon(String, CLLocationCoordinate2D)
}

extension Poking: TargetType {
    public var baseURL: URL {
        return URL(string: "https://pokingdotcom.appspot.com")!
    }
    public var path: String {
        var path = ""
        switch self {
        case .hotel(_):
            path = "nearestHotels"
            // TODO: nearestHotels
        case .pokemon(let pokemonId, _):
            path = "pokemon/\(pokemonId)"
        }
        NSLog("path = \(path)")
        return path
    }
    public var method: Moya.Method {
        return .get
    }
    public var parameters: [String: Any]? {
        // http://localhost:8080/nearestHotels?latitude=25.189&longitude=121.455&radius=8000
        var mutableParameters = [String: Any]()
        switch self {
        case .hotel(let location):
            NSLog("\(location)")
            mutableParameters["latitude"] = location.latitude
            mutableParameters["longitude"] = location.longitude
            
        case .pokemon(let pokemonId, let location):
            mutableParameters["latitude"] = location.latitude
            mutableParameters["longitude"] = location.longitude
            mutableParameters["pokemonId"] = pokemonId
            NSLog("query pokemon id \(pokemonId)")
        }
        mutableParameters["radius"] = 3000 // meters
        return mutableParameters
    }
    public var sampleData: Data { return "test".data(using: String.Encoding.utf8)! }
    public var task: Task { return .request }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}
