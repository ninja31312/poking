//
//  MapViewController.swift
//  Poking
//
//  Created by Ching-Fan Hsieh on 11/26/16.
//  Copyright © 2016 Poking. All rights reserved.
//

import UIKit
import GoogleMaps
import Lottie

struct Hotel {
    var id:String
    var lat:CLLocationDegrees
    var lng:CLLocationDegrees
    var name:String
    var bookingUrl:String
    var imageUrl:String
    var available:Bool
    var score:Float
}

struct POI {
    var id:String
    var lat:CLLocationDegrees
    var lng:CLLocationDegrees
    var key:String
}

let latT = 0.01
let lngT = 0.01

class MapViewController: UIViewController {

    let mapView = GMSMapView()
    let locationManager = CLLocationManager()
    let addressLabel = UILabel() // The label shows the address of selected parking lot.
    var hotels = [String:Hotel]()
    var hotelMarkers = [String: GMSMarker]()
    var POIs = [String:POI]()
    var POIMarkers = [String: GMSMarker]()
    var lastIdlePosition:CLLocationCoordinate2D? = nil;
    var selectedPokemon:PKMonster? = nil;

    var POIGrids = [String: [String]]()
    var loadingView = LAAnimationView.animationNamed("TwitterHeart.json")

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(filter))

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(reload))

        self.title = "Poking"

        mapView.delegate = self
        self.view.addSubview(mapView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|[v0]|", views: mapView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|[v0]|", views: mapView)

        addressLabel.numberOfLines = 0
        addressLabel.textAlignment = .center
        addressLabel.backgroundColor = UIColor.init(white: 1.0, alpha: 0.8)
        mapView.addSubview(addressLabel)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|[v0]|", views: addressLabel)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "V:[v0]|", views: addressLabel)

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    func reload(barButtonItem: UIBarButtonItem?) {
//        if let pokemonId = self.selectedPokemon?.id {
            // TODO: Could Moya callback when multiple request are finished ?
            if let loadingView = loadingView {
                loadingView.alpha = 1.0
                loadingView.backgroundColor = UIColor.init(white: 0.4, alpha: 0.25)
                loadingView.contentMode = .scaleAspectFill
                loadingView.loopAnimation = true;
                self.view.addSubview(loadingView)
                self.view.bringSubview(toFront: loadingView)
                NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|[v0]|", views: loadingView)
                NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|[v0]|", views: loadingView)
                loadingView.play()

                let delayInSeconds = 1.5
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    loadingView.pause()
                    UIView.animate(withDuration: 0.4, animations: { 
                        loadingView.alpha = 0.0
                    }, completion: { (Bool) in
                        loadingView.removeFromSuperview()
                    })
                }
            }
            cleanHotels()
            fetchHotels()
//            fetchPokemons(pokemonId: pokemonId)
//        }
    }

    func filter(barButtonItem: UIBarButtonItem) {
        let monsterListViewController = PKMonsterTableViewController()
        monsterListViewController.dismissCallback = {
            [unowned self] monster in
            if monster?.id != self.selectedPokemon?.id {
                self.cleanPOIs()
                self.cleanHotels()
                self.fetchPokemons(pokemonId: (monster?.id)!)
            }
            self.selectedPokemon = monster
            self.dismiss(animated: true, completion: nil)
//            self.reload(barButtonItem: nil)
        }
        let nc = UINavigationController(rootViewController: monsterListViewController)
        if ((self.presentedViewController) != nil) {
            return
        }
        self.present(nc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult() else {
                return
            }
            guard let lines = address.lines else {
                return
            }
            self.addressLabel.text = lines.joined(separator: "\n")
            let labelHeight = self.addressLabel.intrinsicContentSize.height
            self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                                bottom: labelHeight, right: 0)
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func addHotelMarker(hotelJsonObject:[String: Any]) {
        guard let hotelId = hotelJsonObject["HotelId"] as? String else {
            return
        }
        guard let lat = hotelJsonObject["Latitude"] as? CLLocationDegrees else {
            return
        }
        guard let lng = hotelJsonObject["Longitude"] as? CLLocationDegrees else {
            return
        }
        guard let name = hotelJsonObject["Name"] as? String else {
            return
        }
        guard let bookingUrl = hotelJsonObject["PageUrl"] as? String else {
            return
        }
        guard let imageUrl = hotelJsonObject["PhotoUrl"] as? String else {
            return
        }
        
        guard let score = hotelJsonObject["ReviewScore"] as? Float else {
            return
        }
//        guard let available = hotelJsonObject["available"] as? Bool else {
//            return
//        }

        if score < 8.0 {
            return
        }
        
        let hotel = Hotel(id: hotelId, lat: lat, lng: lng, name: name, bookingUrl: bookingUrl, imageUrl: imageUrl, available: true, score:score)
        
        self.hotels[hotelId] = hotel

        if hotel.available {
            if self.hotelMarkers[hotel.id] == nil {
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = mapView
                marker.userData = hotelId
                marker.title = name
                marker.appearAnimation = kGMSMarkerAnimationPop
                self.hotelMarkers[hotelId] = marker
            }
        } else {
            if let marker = self.hotelMarkers[hotel.id] {
                marker.map = nil    // clean this marker from map
                self.hotelMarkers.removeValue(forKey: hotel.id)
            }
        }
    }

    func addPOIMarker(poiJsonObject:[String: Any]) {
        guard let poiId = poiJsonObject["ID"] as? String else {
            return
        }
        guard let poiLat = poiJsonObject["Latitude"] as? CLLocationDegrees else {
            return
        }
        guard let poiLng = poiJsonObject["Longitude"] as? CLLocationDegrees else {
            return
        }

        let key = "\(poiLat)+\(poiLng)"
        let poi = POI(id: poiId, lat: poiLat, lng: poiLng, key: key)
        self.POIs[key] = poi

        let latN = poiLat / latT
        let lngN = poiLng / lngT
        let latId = Int(floor(latN))
        let lngId = Int(floor(lngN))
        let gridId = "\(latId)+\(lngId)"
        
        NSLog("grid = \(gridId)")

        if self.POIMarkers[key] == nil {
            let marker = GMSMarker(position: CLLocationCoordinate2DMake(poiLat, poiLng))
            marker.map = mapView
            marker.userData = key
            marker.title = self.selectedPokemon?.name
            marker.icon = UIImage(named: (self.selectedPokemon?.imageName)!)
            self.POIMarkers[key] = marker
        }

        if self.POIGrids[gridId] == nil {
            self.POIGrids[gridId] = [String]()
        }

//        var a = Array(self.POIGrids[gridId])
//        a.append("1")
    }

    func fetchHotels() {
        guard let position = self.lastIdlePosition else {
            return
        }
        PokingProvider.request(.hotel(position), completion: {result in
            switch result {
            case let .success(response):
                do {
                    if let json = try response.mapJSON() as? Array<Dictionary<String, Any>> {
                        for item in json {
                            self.addHotelMarker(hotelJsonObject: item)
                        }
                    }
                } catch {
                    NSLog("GGG")
                }
            case let .failure(error):
                NSLog("call api error")
                guard error is CustomStringConvertible else {
                    break;
                }
            }
        })
    }
    
    func cleanHotels() {
        for (_,marker) in self.hotelMarkers {
            marker.map = nil
        }
        self.hotelMarkers.removeAll()
        self.hotels.removeAll()
    }

    func cleanPOIs() {
        for (_,marker) in self.POIMarkers {
            marker.map = nil
        }
        self.POIMarkers.removeAll()
        self.POIs.removeAll()
    }
    
    func fetchPokemons(pokemonId:String) {
        guard let position = self.lastIdlePosition else {
            return
        }
        PokingProvider.request(.pokemon(pokemonId, position), completion: {result in
            switch result {
            case let .success(response):
                do {
                    if let json = try response.mapJSON() as? Array<Dictionary<String, Any>> {
                        for item in json {
                            self.addPOIMarker(poiJsonObject: item)
                        }
                    }
                } catch {
                    NSLog("GG")
                }
            case let .failure(error):
                guard error is CustomStringConvertible else {
                    break;
                }
            }
        })
    }
}

// MARK: - CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

// MARK: - GMSMapViewDelegate

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.lastIdlePosition = position.target
    }

    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        guard (marker.userData as? String) != nil else {
            return nil
        }
        
        let hotelId = marker.userData as? String
        guard let hotel = self.hotels[hotelId!] else {
            return nil
        }
        
        let view = PKInfoView()
        
        view.frame = CGRect(x: 0, y: 0, width: 290, height: 130)
        
        
        // setup info window
        view.imageView.image = UIImage(named: "hotel")    // TODO: replace with hotel's image
        view.titleLabel.text = hotel.name
        view.subtitleLabel.text = "Score: \(hotel.score)"

        return view
    }

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard (marker.userData as? String) != nil else {
            return
        }
        
        let hotelId = marker.userData as? String
        guard let hotel = self.hotels[hotelId!] else {
            return
        }

        var urlString:String
        if hotel.bookingUrl.characters.count == 0 {
            urlString = "https://www.booking.com/"
        } else {
            urlString = hotel.bookingUrl
        }
        let webViewVC = PKWebViewController(url: URL(string:urlString)!)
        //TODO: Fix the leak
        webViewVC.dismissCallback = {
            [unowned self] obj in
            self.dismiss(animated: true, completion: nil)
        }
        let nc = UINavigationController(rootViewController: webViewVC)
        if ((self.presentedViewController) != nil) {
            return
        }
        self.present(nc, animated: true, completion: nil)
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        reverseGeocodeCoordinate(coordinate: marker.position)
        // return false means that continue the default action.
        return false
    }
}
