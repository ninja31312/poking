//
//  PKMonsterTableViewController.swift
//  Poking
//
//  Created by  Ching-Fan Hsieh on 3/11/17.
//  Copyright © 2017 Poking. All rights reserved.
//

import UIKit

struct PKMonster {
    var name: String
    var id: String
    var imageName: String
}

class PKMonsterCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PKMonsterTableViewController: UITableViewController {
    var dismissCallback: ((PKMonster?) -> ())? = nil
    var monsters = [PKMonster]()
    override func viewDidLoad() {
        super.viewDidLoad()
        monsters.append(PKMonster(name: "茸茸羊(Flaaffy)", id: "180", imageName: "180"))
        monsters.append(PKMonster(name: "未知圖騰(Unown)", id: "201", imageName: "201"))
        monsters.append(PKMonster(name: "班基拉斯(Tyranitar)", id: "248", imageName: "248"))
        monsters.append(PKMonster(name: "電龍(Ampharos)", id: "181", imageName: "181"))
        monsters.append(PKMonster(name: "幸福蛋(Blissey)", id: "242", imageName: "242"))
        monsters.append(PKMonster(name: "沙基拉斯(Pupitar)", id: "247", imageName: "247"))
        monsters.append(PKMonster(name: "大奶罐(Miltank)", id: "241", imageName: "241"))
        monsters.append(PKMonster(name: "戰舞郎(Hitmontop)", id: "237", imageName: "237"))
        monsters.append(PKMonster(name: "火爆獸(Typhlosion)", id: "157", imageName: "157"))
        monsters.append(PKMonster(name: "大力鱷(Feraligatr)", id: "160", imageName: "160"))
        self.tableView.register(PKMonsterCell.classForCoder(), forCellReuseIdentifier: String(describing: PKMonsterCell.self))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
    }

    func cancel(barButtonItem: UIBarButtonItem) {
        if let dismissCallback = dismissCallback {
            dismissCallback(nil)
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monsters.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PKMonsterCell.self), for: indexPath)
        let monster = monsters[indexPath.row]
        cell.textLabel?.text = monster.name
        cell.imageView?.image = UIImage(named:monster.imageName)
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dismissCallback = dismissCallback {
            let monster = monsters[indexPath.row]
            dismissCallback(monster)
        }
    }
}

