//
//  NSLayoutConstraint+SimpleVisualFormat.swift
//  Poking
//
//  Created by Ching-Fan Hsieh on 11/26/16.
//  Copyright © 2016 Poking. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    static func activateConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
