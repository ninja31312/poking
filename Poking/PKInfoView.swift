//
//  PKInfoView.swift
//  Poking
//
//  Created by  Ching-Fan Hsieh on 3/11/17.
//  Copyright © 2017 Poking. All rights reserved.
//

import UIKit

class PKInfoView: UIView {
    let imageView = UIImageView()
    let titleLabel = UILabel()
    let availabilityLabel = UILabel()
    let subtitleLabel = UILabel()
    let bookButton = UIButton()
    override init (frame : CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true

        // Image
        imageView.image = UIImage(named: "hotel")
        self.addSubview(imageView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:[v0(60)]", views: imageView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|-5-[v0(60)]", views: imageView)
        // Title
        titleLabel.numberOfLines = 1
        titleLabel.text = "W Hotel"
        self.addSubview(titleLabel)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|-5-[v0]-10-[v1]|", views: imageView, titleLabel)
        // Available rooms
        availabilityLabel.numberOfLines = 1
//        let availableRooms = 100
        availabilityLabel.text = "Available now!"
        availabilityLabel.textColor = UIColor.orange
        self.addSubview(availabilityLabel)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|-5-[v0]-10-[v1]|", views: imageView, availabilityLabel)
        // Subtitle
        subtitleLabel.numberOfLines = 1
        subtitleLabel.text = "Thid hotel is amazing."
        self.addSubview(subtitleLabel)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|-5-[v0]-10-[v1]|", views: imageView, subtitleLabel)
        // Button
        bookButton.setTitle("Book", for: .normal)
        bookButton.setTitleColor(UIColor.init(red: 30/255, green: 144/255, blue: 255/255, alpha: 1), for: .normal)
        self.addSubview(bookButton)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:[v0]-5-|", views: bookButton)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|-5-[v0]-5-[v1]-5-[v2]-5-[v3]|", views: titleLabel, availabilityLabel, subtitleLabel, bookButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
