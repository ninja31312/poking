//
//  PKWebViewController.swift
//  Poking
//
//  Created by  Ching-Fan Hsieh on 3/11/17.
//  Copyright © 2017 Poking. All rights reserved.
//

import UIKit
import WebKit
import Lottie

class PKWebViewController: UIViewController {
    let webView = WKWebView()
    var url:URL? = nil
    var dismissCallback: (() -> ())? = nil
    var loadingView = LAAnimationView.animationNamed("TwitterHeart.json")

    deinit {
        
    }
    init(url: URL) {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = UIColor.white
        webView.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        self.url = url
        let request = URLRequest(url: url as URL)
        webView.load(request)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|[v0]|", views: webView)
        NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|[v0]|", views: webView)
        if let loadingView = loadingView {
            loadingView.backgroundColor = UIColor.white
            loadingView.alpha = 1.0
            loadingView.contentMode = .scaleAspectFill
            loadingView.loopAnimation = true;
            self.view.addSubview(loadingView)
            self.view.bringSubview(toFront: loadingView)
            NSLayoutConstraint.activateConstraintsWithFormat(format: "H:|[v0]|", views: loadingView)
            NSLayoutConstraint.activateConstraintsWithFormat(format: "V:|[v0]|", views: loadingView)
            loadingView.play()
        }
    }
    func cancel(barButtonItem: UIBarButtonItem) {
        if let dismissCallback = dismissCallback {
            dismissCallback()
        }
    }
}


extension PKWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let loadingView = loadingView {
            loadingView.pause()
            UIView.animate(withDuration: 0.4, animations: {
                loadingView.alpha = 0.0
            }) { (Bool) in
                loadingView.removeFromSuperview()
            }
        }
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        if let loadingView = loadingView {
            loadingView.pause()
            UIView.animate(withDuration: 0.4, animations: {
                loadingView.alpha = 0.0
            }) { (Bool) in
                loadingView.removeFromSuperview()
            }
        }
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        if let loadingView = loadingView {
            loadingView.pause()
            UIView.animate(withDuration: 0.4, animations: {
                loadingView.alpha = 0.0
            }) { (Bool) in
                loadingView.removeFromSuperview()
            }
        }
    }
}
